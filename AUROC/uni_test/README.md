# AUROC单元测试

## 内容
目前包含四个模块测试文件和一个配置文件夹。
1. 模块测试文件：包括dataloader、loss、metrics、models，可以按需求增加，建议按模块独立性划分。
2. 配置文件夹：即`configs`，包含dataset、loss、model三类，可自由组合用于替换`base_config.json`内的配置，实现尽可能覆盖所有可能的配置。仅用于测试，正式发布版本不提供config调用。

## 单元测试用法
1. 安装pytest
```
pip3 install pytest
```
2. 在`uni_test`的上级目录，即`XCurveOpt/AUROC`下调用
```
pytest uni_test
```
即可执行所有测试，修改代码后，在merge至master分支前必须通过测试，避免修改代码影响其他部分。
3. 测试所有代码较慢，在调试过程中可以执行单个模块测试，例如
```
pytest uni_test/test_metrics.py
```
在单个模块内也可以通过`@pytest.mark.skip()`修饰不想测试的函数，或者暂时增加调试信息，直接修改`main`函数然后执行
```
python uni_test/test_metrics.py
```
查看调试信息。

## 测试代码编写注意事项
1. 所有测试函数需以`test_`开头，非测试函数用`@pytest.mark.skip()`修饰。
2. 判断代码是否通过可用`assert`或`raise Error`实现，不可输出结果后人工检查。
3. 尽可能提高测试覆盖率，例如待开放的函数中涉及到的可选接口、数据类型、模型类型、损失函数类型等，可以用config文件的模块化组装实现各种组合；对于同一功能的多种代码实现（如加速代码）需要检查加速前后的一致性，能想到的corner case尽可能都写上。
4. 测试代码也可能写错，测试不通过时首先检查测试代码是不是不合理，尤其是新增的测试。
