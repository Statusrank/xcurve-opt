import pytest
import json
import sys
from easydict import EasyDict as edict

sys.path.append("./")
from dataloaders import get_datasets


def load_json(dataset=None):
    with open('./uni_test/configs/_base_config.json', 'r') as f:
        args = json.load(f)
        args = edict(args)

    if dataset is not None:
        with open('./uni_test/configs/%s.json'%dataset, 'r') as f:
            args.update(edict(json.load(f)))

    return args

def gen_lmdb():
    for dataset in ['cifar-10-long-tail', 'cifar-100-long-tail', \
            'tiny-imagenet-200', 'imagenet-lt']:
        args = load_json(dataset)
        train_set, val_set, test_set = get_datasets(args)
        pass

if __name__ == '__main__':
    gen_lmdb()
